package test;

import static org.junit.jupiter.api.Assertions.*;

import java.sql.SQLException;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import database.MariaDBConnection;
import database.StructLoader;

class TestLoad {
	
	private MariaDBConnection conn;
	
	@BeforeEach
	void setUp() throws Exception {
		conn=new MariaDBConnection();
		conn.setDatabase("cpoa");
		conn.setHost("localhost");
		conn.setUsername("cpoa");
		conn.setPassword("cpoa");
		assertNotNull(conn.getConnection());
	}

	@Test
	void testLoad() throws SQLException {
		@SuppressWarnings("unused")
		StructLoader loader=new StructLoader(conn);
	}

}
