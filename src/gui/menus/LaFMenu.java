package gui.menus;

import javax.swing.JMenu;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import gui.actions.LaFAction;

import static gui.RessourceManager.*;

public class LaFMenu extends JMenu {
	/**
	 * 
	 */
	private static final long serialVersionUID = 315947909501537668L;

	public LaFMenu() {
		super(text("menu.laf"));
		
		for(LookAndFeelInfo lf:UIManager.getInstalledLookAndFeels()) {
			String name=lf.getName();
			String cName=lf.getClassName();
			this.add(new LaFAction(name, cName));
		}
	}
}
