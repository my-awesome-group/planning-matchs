package gui.menus;

import javax.swing.JMenuBar;

public class MainMenu extends JMenuBar {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4726704838031944200L;

	public MainMenu() {
		super();
		
		this.add(new FileMenu());
		this.add(new LaFMenu());
	}
}
