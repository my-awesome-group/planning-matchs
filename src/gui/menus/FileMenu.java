package gui.menus;

import javax.swing.JMenu;

import gui.Main;

import static gui.RessourceManager.*;

public class FileMenu extends JMenu {
	/**
	 * 
	 */
	private static final long serialVersionUID = 6746823222621707639L;

	public FileMenu() {
		super(text("menu.file"));
		
		this.add(Main.actions.get("singles"));
		this.add(Main.actions.get("doubles"));
		this.add(Main.actions.get("exit"));
	}
}
