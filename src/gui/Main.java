package gui;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javax.swing.Action;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import database.DBConnection;
import database.MariaDBConnection;
import database.StructLoader;
import gui.windows.MainFrame;

public class Main {
	
	public static DBConnection conn;
	public static StructLoader structs;
	public static Map<String, Action> actions=new HashMap<>();
	public static MainFrame mainFrame;
	
	public static void main(String[] args) {
		connectDb();
		
		SwingUtilities.invokeLater(() -> {
			try {
				UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
			} catch(Exception e) {
				try {
					UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
				} catch(Exception ex) {}
			}
		});
		
		SwingUtilities.invokeLater(() -> {
			mainFrame=new MainFrame();
			mainFrame.setVisible(true);
		});
	}
	
	private static void connectDb() {
		File jar=new File(Main.class.getProtectionDomain().getCodeSource().getLocation().getFile());
		File dbFile;
		if(jar.getName().endsWith(".jar")) {
			dbFile=new File(jar.getParentFile(), "db.properties");
		} else {
			dbFile=new File("db.properties");
		}
		if(dbFile.exists()) {
			try {
				conn=new MariaDBConnection(dbFile);
				conn.getConnection();
			} catch(Exception e) {
				e.printStackTrace();
				JOptionPane.showMessageDialog(null, "Failed to connect to database: "+e.getMessage(), "database connection error", JOptionPane.ERROR_MESSAGE);
				System.exit(1);
			}
		} else {
			JOptionPane.showMessageDialog(null, "Database config file not found at "+dbFile.getAbsolutePath(), "database connection error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
		try {
			structs=new StructLoader(conn);
		} catch(Exception e) {
			JOptionPane.showMessageDialog(null, "Failed to load database: "+e.getMessage()+"\n"+e.toString(), "database load error", JOptionPane.ERROR_MESSAGE);
			System.exit(1);
		}
	}

}
