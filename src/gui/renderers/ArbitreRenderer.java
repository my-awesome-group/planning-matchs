package gui.renderers;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.ListCellRenderer;

import struct.Arbitre;

public class ArbitreRenderer implements ListCellRenderer<Arbitre> {

	@Override
	public Component getListCellRendererComponent(JList<? extends Arbitre> list, Arbitre value, int index, boolean isSelected, boolean cellHasFocus) {
		JPanel p=new JPanel();
		p.setLayout(new BorderLayout());
		p.add(new JLabel("<html><strong>"+value.getPrenom()+" "+value.getNom()+"</strong></html>"), BorderLayout.CENTER);
		p.add(new JLabel("["+value.getNationalite()+"]"), BorderLayout.EAST);
		if(isSelected) p.setBackground(Color.BLUE.brighter().brighter().brighter());
		if(cellHasFocus) p.setBackground(Color.BLUE);
		return p;
	}

}
