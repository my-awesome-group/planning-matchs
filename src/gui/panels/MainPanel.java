package gui.panels;

import java.awt.CardLayout;

import javax.swing.JPanel;

public class MainPanel extends JPanel {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5596920994565544908L;
	private CardLayout layout;
	
	public MainPanel() {
		super();
		
		this.setOpaque(true);
		layout=new CardLayout();
		this.setLayout(layout);
		
		this.add(new SinglesPanel(), "singles");
		this.add(new DoublesPanel(), "doubles");
	}
	
	public void showTab(String tab) {
		layout.show(this, tab);
	}
}
