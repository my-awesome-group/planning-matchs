package gui.panels;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;

public class DoublesPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4257938825899185873L;

	public DoublesPanel() {
		super();
		
		this.setBackground(Color.RED);
		this.setMinimumSize(new Dimension(100, 100));
	}
}
