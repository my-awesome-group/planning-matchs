package gui.panels;

import java.awt.GridLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;

import gui.actions.DialogCloseAction;

public class DialogOptionsPanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1666135925290076038L;

	
	public DialogOptionsPanel(JDialog dialog) {
		setLayout(new GridLayout(1, 2));
		add(new JButton(new DialogCloseAction(dialog)));
	}
}
