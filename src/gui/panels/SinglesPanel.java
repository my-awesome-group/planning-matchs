package gui.panels;

import javax.swing.JPanel;

import gui.entities.MatchEntity;
import gui.layouts.BracketLayout;

import gui.layouts.BracketLayout.Position;

public class SinglesPanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4477119696265486576L;

	public SinglesPanel() {
		super();
		
		BracketLayout layout=new BracketLayout(5, 30, 10, 200, 50);
		this.setLayout(layout);
		int id=0;
		for(int i=0; i<5; i++) {
			for(int j=0; j<layout.layerCount(i); j++) {
				this.add(MatchEntity.getSingleOrPopulate(id++).summary(), new Position(i, j));
			}
		}
	}
}
