package gui.windows;

import java.awt.Window;
import java.util.Comparator;
import java.util.function.Predicate;

import javax.swing.JComboBox;

import gui.Main;
import gui.renderers.ArbitreRenderer;
import struct.Arbitre;
import util.FunctionalList;
import util.FunctionalMap;
import static gui.RessourceManager.*;

public class ArbitreGetterDialog extends GetterDialog<Arbitre> {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7469349637269190170L;
	Predicate<Arbitre> pred;

	public ArbitreGetterDialog(Window parent, Predicate<Arbitre> pred) {
		super(parent);
		this.pred=pred;
		build();
		finish();
	}
	public ArbitreGetterDialog(Window parent) {
		this(parent, a -> true);
	}
	
	
	private void build() {
		setTitle(text("getter.arbitre"));
		JComboBox<Arbitre> comboBox=new JComboBox<>();
		comboBox.setRenderer(new ArbitreRenderer());
		FunctionalList.forEach(FunctionalList.filter(FunctionalMap.sortToList(Main.structs.getArbitres(), 
			Comparator.comparing(Arbitre::getNom).thenComparing(Comparator.comparing(Arbitre::getPrenom))
		), pred), comboBox::addItem);
		value=(Arbitre) comboBox.getItemAt(0);
		comboBox.addItemListener(e -> value=(Arbitre) comboBox.getSelectedItem()); 
		add(comboBox);
	}
}
