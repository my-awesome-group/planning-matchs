package gui.windows;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Window;
import java.awt.event.KeyEvent;

import javax.swing.JDialog;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

import gui.actions.ArbitraryAction;
import gui.panels.DialogOptionsPanel;

public abstract class BaseDialog extends JDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2832531764531238994L;

	public BaseDialog(Window parent) {
		super(parent, Dialog.ModalityType.APPLICATION_MODAL);
		build();
	}
	
	private void build() {
		setLayout(new BorderLayout());
		add(new DialogOptionsPanel(this), BorderLayout.SOUTH);
		getRootPane().getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "closeDialog");
		getRootPane().getActionMap().put("closeDialog", new ArbitraryAction(e -> {
			SwingUtilities.getWindowAncestor((Component) e.getSource()).dispose();
		}));
	}
	protected void finish() {
		pack();
		setResizable(false);
		setLocationRelativeTo(getParent());
	}
	
	public void update() {
		dispose();
		finish();
		setVisible(true);
	}
}
