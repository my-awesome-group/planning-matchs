package gui.windows;

import gui.Main;
import gui.entities.MatchEntity;
import util.MapUtil;

import static gui.RessourceManager.*;

import java.awt.BorderLayout;

public class MatchDialog extends BaseDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3554663819841734042L;

	public MatchDialog(MatchEntity m) {
		super(Main.mainFrame);
		setTitle(text("matchwindow.title", MapUtil.build(String.class, Object.class, "id", m.getMatch().getIdMatch())));
		setIconImage(iconAsImage("vs.png"));
		add(m.details(), BorderLayout.CENTER);
		finish();
	}
}
