package gui.windows;

import java.awt.Window;

public abstract class GetterDialog<T> extends BaseDialog {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6359916338032727536L;
	
	public GetterDialog(Window parent) {
		super(parent);
	}
	
	protected T value;
	
	public T get(T defaultValue) {
		value=defaultValue;
		return get();
	}
	
	protected void populate() {}

	public T get() {
		populate();
		setVisible(true);
		return value;
	}
}
