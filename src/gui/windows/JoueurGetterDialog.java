package gui.windows;

import java.awt.Window;
import java.util.Comparator;
import java.util.function.Predicate;

import javax.swing.JComboBox;

import gui.Main;
import gui.renderers.JoueurRenderer;
import struct.Joueur;
import util.FunctionalList;
import util.FunctionalMap;
import static gui.RessourceManager.*;

public class JoueurGetterDialog extends GetterDialog<Joueur> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8382560665225834973L;
	
	Predicate<Joueur> pred;

	public JoueurGetterDialog(Window parent, Predicate<Joueur> pred) {
		super(parent);
		this.pred=pred;
		build();
		finish();
	}
	public JoueurGetterDialog(Window parent) {
		this(parent, a -> true);
	}
	
	
	private void build() {
		setTitle(text("getter.joueur"));
		JComboBox<Joueur> comboBox=new JComboBox<>();
		comboBox.setRenderer(new JoueurRenderer());
		FunctionalList.forEach(FunctionalList.filter(FunctionalMap.sortToList(Main.structs.getJoueurs(), 
			Comparator.comparing(Joueur::getNom).thenComparing(Comparator.comparing(Joueur::getPrenom))
		), pred), comboBox::addItem);
		value=(Joueur) comboBox.getItemAt(0);
		comboBox.addItemListener(e -> value=(Joueur) comboBox.getSelectedItem()); 
		add(comboBox);
	}
}
