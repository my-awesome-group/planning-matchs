package gui.windows;

import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

import gui.Main;
import gui.actions.ExitAction;
import gui.actions.TabAction;
import gui.menus.MainMenu;
import gui.panels.MainPanel;

import static gui.RessourceManager.*;

public class MainFrame extends JFrame implements WindowListener {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1117118197840945237L;
	private MainPanel panel;
	
	public MainFrame() {
		super();
		
		this.setTitle(text("mainwindow.title"));
		this.addWindowListener(this);
		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.setIconImage(iconAsImage("tennis.png"));
		
		this.loadActions();
		this.loadContent();
		this.pack();
		this.setLocationRelativeTo(null);
		this.setResizable(false);
	}
	
	private void loadActions() {
		Main.actions.put("exit", new ExitAction());
		Main.actions.put("singles", new TabAction("singles", "menu.file.singles"));
		Main.actions.put("doubles", new TabAction("doubles", "menu.file.doubles"));
	}
	
	private void loadContent() {
		panel=new MainPanel();
		this.setContentPane(panel);
		this.setJMenuBar(new MainMenu());
	}
	
	public MainPanel getPanel() {
		return panel;
	}

	@Override
	public void windowClosing(WindowEvent e) {
		if(JOptionPane.showConfirmDialog(this, text("mainwindow.quitconfirm"), text("mainwindow.quitconfirmtitle"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE)==JOptionPane.YES_OPTION) {
			this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			this.dispose();
		}
	}

	@Override public void windowOpened(WindowEvent e) {}
	@Override public void windowClosed(WindowEvent e) {}
	@Override public void windowIconified(WindowEvent e) {}
	@Override public void windowDeiconified(WindowEvent e) {}
	@Override public void windowActivated(WindowEvent e) {}
	@Override public void windowDeactivated(WindowEvent e) {}
}
