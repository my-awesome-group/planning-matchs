package gui.layouts;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.LayoutManager2;
import java.awt.Point;
import java.awt.Rectangle;

public class MatchSummaryLayout implements LayoutManager, LayoutManager2 {
	
	private Component id, score, playersTop, playersBottom;
	
	public static String ID="id";
	public static String SCORE="score";
	public static String PLAYERS_TOP="top";
	public static String PLAYERS_BOTTOM="bottom";
	
	@Override
	@Deprecated
	public void addLayoutComponent(String name, Component comp) {
		addLayoutComponent(comp, name);
	}

	@Override
	public void removeLayoutComponent(Component comp) {
		if(id==comp) id=null;
		if(score==comp) score=null;
		if(playersTop==comp) playersTop=null;
		if(playersBottom==comp) playersBottom=null;
	}

	@Override
	public Dimension preferredLayoutSize(Container parent) {
		int width, height;
		height=Math.max(playersTop.getPreferredSize().height,playersBottom.getPreferredSize().height)*2+9;
		width=Math.max(playersTop.getPreferredSize().width, playersBottom.getPreferredSize().width)+score.getPreferredSize().width+9;
		return new Dimension(width, height);
	}

	@Override
	public Dimension minimumLayoutSize(Container parent) {
		return preferredLayoutSize(parent);
	}

	@Override
	public void layoutContainer(Container parent) {
		Dimension size=parent.getSize();
		
		Dimension topSize=playersTop.getPreferredSize();
		Dimension bottomSize=playersBottom.getPreferredSize();
		Dimension playersSize=new Dimension(Math.max(topSize.width, bottomSize.width), Math.max(topSize.height, bottomSize.height));	
		
		playersTop.setBounds(new Rectangle(new Point(2, 2), playersSize));
		playersBottom.setBounds(new Rectangle(new Point(2, size.height-playersSize.height-2), playersSize));
		
		Dimension scoreSize=score.getPreferredSize();
		score.setBounds(new Rectangle(new Point(size.width-scoreSize.width-2, (size.height-scoreSize.height)/2), scoreSize));
		
		Dimension idSize=id.getPreferredSize();
		id.setBounds(new Rectangle(new Point(size.width-idSize.width-2, 2), idSize));
	}

	@Override
	public void addLayoutComponent(Component comp, Object constraints) {
		if(constraints.equals(ID)) id=comp;
		else if(constraints.equals(SCORE)) score=comp;
		else if(constraints.equals(PLAYERS_TOP)) playersTop=comp;
		else if(constraints.equals(PLAYERS_BOTTOM)) playersBottom=comp;
		else throw new IllegalArgumentException("Invalid constrtaints "+constraints);
	}

	@Override
	public Dimension maximumLayoutSize(Container target) {
		return preferredLayoutSize(target);
	}

	@Override
	public float getLayoutAlignmentX(Container target) {
		return 0;
	}

	@Override
	public float getLayoutAlignmentY(Container target) {
		return 0;
	}

	@Override
	public void invalidateLayout(Container target) {}

}
