package gui.layouts;

import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.LayoutManager;
import java.awt.LayoutManager2;
import java.awt.Rectangle;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

public class BracketLayout implements LayoutManager, LayoutManager2 {
	
	public static class Position {
		public int round;
		public int index;
		
		public Position() {}
		public Position(int round, int index) {
			this.round=round;
			this.index=index;
		}
		
		@Override
		public int hashCode() {
			return index*65537+round;
		}
		
		@Override
		public String toString() {
			return String.format("[round %d, index %d]", round, index); 
		}
		
		@Override
		public boolean equals(Object other) {
			if(this==other) return true;
			if(other.getClass()!=this.getClass()) return false;
			Position p=(Position) other;
			return p.round==round && p.index==index;
		}
	}

	private int rounds;
	private int layers;
	private Map<Position, Component> matches;
	
	private int hgap, vgap;
	private int componentW, componentH;
	
	public BracketLayout(int rounds, int hgap, int vgap, int componentW, int componentH) {
		this.rounds=rounds;
		this.layers=layerCount(0);
		this.matches=new HashMap<>();
		
		this.hgap=hgap;
		this.vgap=vgap;
		this.componentW=componentW;
		this.componentH=componentH;
	}
	
	public int layerCount(int round) {
		return 1<<(this.rounds-round-1);
	}

	@Override
	public void addLayoutComponent(Component comp, Object constraints) {
		Position pos=(Position) constraints;
		if(pos.round<0 || pos.round>=this.rounds) throw new IllegalArgumentException("Invalid round");
		if(pos.index<0 || pos.index>=layerCount(pos.round-1)) throw new IllegalArgumentException("Invalid position: "+pos);
		this.matches.put(pos, comp);
	}

	@Override
	public Dimension maximumLayoutSize(Container target) {
		return this.preferredLayoutSize(target);
	}

	@Override
	public float getLayoutAlignmentX(Container target) {
		return 0;
	}

	@Override
	public float getLayoutAlignmentY(Container target) {
		return 0;
	}

	@Override
	public void invalidateLayout(Container target) {}

	@Override
	@Deprecated
	public void addLayoutComponent(String name, Component comp) {
		throw new RuntimeException("container.add(String, Component) not implemented");
	}

	@Override
	public void removeLayoutComponent(Component comp) {
		Position p=null;
		for(Entry<Position, Component> e:this.matches.entrySet()) {
			if(e.getValue().equals(comp)) p=e.getKey();
		}
		if(p==null) throw new IllegalArgumentException("Container not present");
		this.matches.remove(p);
	}

	@Override
	public Dimension preferredLayoutSize(Container parent) {
		return minimumLayoutSize(parent);
	}

	@Override
	public Dimension minimumLayoutSize(Container parent) {
		int width=this.hgap*(this.rounds-1)+this.componentW*this.rounds;
		int height=this.vgap*(this.layers-1)+this.componentH*this.layers;
		return new Dimension(width, height);
	}

	@Override
	public void layoutContainer(Container parent) {
		Dimension totalSize=minimumLayoutSize(parent);
		int width=totalSize.width;
		int height=totalSize.height;
		
		Dimension realSize=parent.getSize();
		int offsetX=(realSize.width-width)/2;
		int offsetY=(realSize.height-height)/2;
		
		double currentX=offsetX;
		
		for(int round=0; round<this.rounds; round++) {
			int layerCount=layerCount(round);
			double totalGapHeight=height-layerCount*componentH;
			double gapHeight=totalGapHeight/(2*layerCount);
			
			double currentY=gapHeight+offsetY;
			for(int layer=0; layer<layerCount; layer++) {
				Component c=this.matches.get(new Position(round, layer));
				if(c!=null) c.setBounds(new Rectangle((int) currentX, (int) currentY, componentW, componentH));
				currentY+=2*gapHeight+componentH;
			}
			currentX+=componentW+hgap;
		}
	}

}
