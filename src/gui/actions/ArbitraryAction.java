package gui.actions;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;

public class ArbitraryAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8539549215213046366L;

	public ArbitraryAction(String name, ActionListener l) {
		super(name);
		putValue("listener", l);
	}

	public ArbitraryAction(ActionListener l) {
		super();
		putValue("listener", l);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		((ActionListener) getValue("listener")).actionPerformed(e);
	}

}
