package gui.actions;

import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import javax.swing.AbstractAction;
import javax.swing.SwingUtilities;

import gui.entities.MatchEntity.DetailsPanel;
import gui.windows.JoueurGetterDialog;
import struct.Joueur;
import struct.MatchSimple;

import static gui.RessourceManager.*;

public class PlayerEditAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3573346683739232684L;

	public PlayerEditAction(DetailsPanel pnl, int idx) {
		super(text("matchwindow.editplayer"));
		putValue("pnl", pnl);
		putValue("idx", idx);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		DetailsPanel pnl=(DetailsPanel) getValue("pnl");
		MatchSimple m=(MatchSimple) pnl.getMatch();
		List<Joueur> j=new ArrayList<>();
		if(m.getJoueurs()!=null) {
			for(Joueur a:m.getJoueurs()) j.add(a);
		}
		Joueur n=new JoueurGetterDialog(SwingUtilities.getWindowAncestor(pnl), ((Predicate<Joueur>) j::contains).negate()).get();
		if(n!=null) {
			Joueur[] q=m.getJoueurs();
			if(q==null) q=new Joueur[2];
			q[(Integer) getValue("idx")]=n;
			m.setJoueurs(q);
		}
		pnl.update();
	}

}
