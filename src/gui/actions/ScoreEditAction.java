package gui.actions;

import static gui.RessourceManager.text;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import gui.entities.MatchEntity.DetailsPanel;

public class ScoreEditAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6348305891357798917L;
	
	public ScoreEditAction(DetailsPanel pnl) {
		super(text("matchwindow.editscore"));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}

}
