package gui.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import gui.Main;

import static gui.RessourceManager.*;

public class TabAction extends AbstractAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7687407840507211607L;

	public TabAction(String tab, String name) {
		super(text(name));
		this.putValue("tab", tab);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Main.mainFrame.getPanel().showTab((String) this.getValue("tab"));
	}

}
