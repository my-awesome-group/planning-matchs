package gui.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import gui.entities.MatchEntity.DetailsPanel;

import static gui.RessourceManager.text;
public class DateTimeEditAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1816706022144014262L;

	public DateTimeEditAction(DetailsPanel pnl) {
		super(text("matchwindow.editdatetime"));
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		// TODO Auto-generated method stub

	}

}
