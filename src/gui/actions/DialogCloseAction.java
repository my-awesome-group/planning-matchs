package gui.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.JDialog;
import static gui.RessourceManager.*;

public class DialogCloseAction extends AbstractAction {
	
	private static final long serialVersionUID = -3827731113033772729L;

	public DialogCloseAction(JDialog dialog) {
		super(text("dialog.actions.close"));
		putValue("dialog", dialog);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		((JDialog) getValue("dialog")).dispose();
	}

}
