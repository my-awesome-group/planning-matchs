package gui.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;

import gui.Main;

public class LaFAction extends AbstractAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -3129383889884130009L;

	public LaFAction(String name, String cName) {
		super(name);
		this.putValue("class", cName);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		try {
			UIManager.setLookAndFeel((String) this.getValue("class"));
			SwingUtilities.updateComponentTreeUI(Main.mainFrame);
			Main.mainFrame.pack();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
