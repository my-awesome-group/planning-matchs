package gui.actions;

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;

import gui.Main;

import static gui.RessourceManager.*;

public class ExitAction extends AbstractAction {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2313374030062296710L;

	public ExitAction() {
		super(text("menu.file.exit"));
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Main.mainFrame.windowClosing(null);
	}

}
