package gui.actions;

import java.awt.event.ActionEvent;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import javax.swing.AbstractAction;
import javax.swing.SwingUtilities;

import gui.entities.MatchEntity.DetailsPanel;
import gui.windows.ArbitreGetterDialog;
import struct.ArbitreChaise;
import struct.Joueur;
import struct.Match;
import util.FunctionalList;

import static gui.RessourceManager.text;
public class ArbitreEditAction extends AbstractAction {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1816706022144014262L;

	public ArbitreEditAction(DetailsPanel pnl) {
		super(text("matchwindow.editarbitre"));
		putValue("pnl", pnl);
	}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		DetailsPanel pnl=(DetailsPanel) getValue("pnl");
		Match m=pnl.getMatch();
		List<String> n=m.getJoueurs()==null?Collections.emptyList():FunctionalList.map(Arrays.asList(m.getJoueurs()), Joueur::getNationalite);
		ArbitreChaise a=(ArbitreChaise) new ArbitreGetterDialog(SwingUtilities.getWindowAncestor(pnl), x -> {
			if(!(x instanceof ArbitreChaise)) return false;
			return !n.contains(x.getNationalite());
		}).get();
		if(a!=null) m.setArbitreChaise(a);
		pnl.update();
	}

}
