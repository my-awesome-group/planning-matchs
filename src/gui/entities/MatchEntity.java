package gui.entities;

import java.awt.Color;
import java.awt.GridLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;

import gui.Main;
import gui.actions.ArbitreEditAction;
import gui.actions.DateTimeEditAction;
import gui.actions.EquipeRamasseursEditAction;
import gui.actions.PlayerDuoEditAction;
import gui.actions.PlayerEditAction;
import gui.actions.ScoreEditAction;
import gui.layouts.MatchSummaryLayout;
import gui.windows.BaseDialog;
import gui.windows.MatchDialog;
import struct.DuoJoueur;
import struct.Joueur;
import struct.Match;
import struct.MatchDouble;
import struct.MatchSimple;
import util.FunctionalList;
import util.FunctionalMap;
import util.ListUtil;
import util.MapUtil;
import util.StringUtil;

import static gui.RessourceManager.*;

public class MatchEntity {
	public class DetailsPanel extends JPanel {
		private static final long serialVersionUID = 156094465572991962L;
		
		private JLabel player0, player1, score, dateTime, arbitre, equipeRamasseurs;
		
		public DetailsPanel() {
			super();
			setBackground(Color.GREEN);
			setLayout(new GridLayout(7, 2));
			
			if(match instanceof MatchSimple) {
				player0=formatPlayers(0);
				add(player0);
				player1=formatPlayers(1);
				add(player1);
				add(new JButton(new PlayerEditAction(this, 0)));
				add(new JButton(new PlayerEditAction(this, 1)));
			} else {
				player0=formatPlayers(0);
				add(player0);
				player1=formatPlayers(1);
				add(player1);
				add(new JButton(new PlayerDuoEditAction(this, 0)));
				add(new JButton(new PlayerDuoEditAction(this, 1)));
			}
			score=formatScore();
			add(score);
			add(new JButton(new ScoreEditAction(this)));
			dateTime=formatDateTime();
			add(dateTime);
			add(new JButton(new DateTimeEditAction(this)));
			arbitre=formatArbitre();
			add(arbitre);
			add(new JButton(new ArbitreEditAction(this)));
			equipeRamasseurs=formatEquipeRamasseurs();
			add(equipeRamasseurs);
			add(new JButton(new EquipeRamasseursEditAction(this)));
		}
		
		private String formatScoreString() {
			if(match.getScore()==null || match.getScore().length==0) return text("placeholder.score");
			return "<html>"+ListUtil.concat(FunctionalList.map(Arrays.asList(match.getScore()), set -> {
				int a=set[0];
				int b=set[1];
				if(a>b) return "<strong>"+a+"</strong>-"+b;
				else return a+"-<strong>"+b+"</strong>";
			}), " ")+"</html>";
		}
		
		private JLabel formatScore() {
			return new JLabel(formatScoreString());
		}
		
		private String formatTimeString() {
			switch(match.getHoraire()) {
			case 0:
				return text("placeholder.time");
			case 1:
			case 2:
			case 3:
			case 4:
				return text("enum.time."+match.getHoraire());
			}
			throw new IllegalStateException("Horaire invalide: "+match.getHoraire());
		}
		
		private String formatDateString() {
			if(match.getDate()==null) return text("placeholder.date");
			Calendar cal=Calendar.getInstance();
			cal.setTime(match.getDate());
			return StringUtil.zeropad(cal.get(Calendar.DAY_OF_MONTH), 2)+"/"+StringUtil.zeropad(cal.get(Calendar.MONTH), 2);
		}
		
		private String formatDateTimeString() {
			return formatDateString()+" "+formatTimeString();
		}
		
		private JLabel formatDateTime() {
			return new JLabel(formatDateTimeString());
		}
		
		private String formatArbitreString() {
			if(match.getArbitreChaise()==null) return text("placeholder.arbitre");
			return match.getArbitreChaise().getNom()+" ["+match.getArbitreChaise().getNationalite()+"]";
		}
		
		private JLabel formatArbitre() {
			return new JLabel(formatArbitreString());
		}
		
		private String formatEquipeRamasseursString() {
			if(match.getEquipeRamasseurs()==null) return text("placeholder.equiperamasseurs");
			return text("matchwindow.fetcherteam", MapUtil.build(String.class, Integer.class, "id", match.getEquipeRamasseurs().getIdEquipeRamasseur()));
		}
		
		private JLabel formatEquipeRamasseurs() {
			return new JLabel(formatEquipeRamasseursString());
		}

		public Match getMatch() {
			return match;
		}
		public void update() {
			player0.setText(formatPlayersString(0));
			player1.setText(formatPlayersString(1));
			score.setText(formatScoreString());
			dateTime.setText(formatDateTimeString());
			arbitre.setText(formatArbitreString());
			equipeRamasseurs.setText(formatEquipeRamasseursString());
			((BaseDialog) SwingUtilities.windowForComponent(this)).update();
		}
	}
	
	private class SummaryPanel extends JPanel implements MouseListener {
		private static final long serialVersionUID = 5746304677439471425L;

		public SummaryPanel() {
			super();
			setBorder(BorderFactory.createLineBorder(Color.BLACK, 1));
			setBackground(Color.GREEN);
			setLayout(new MatchSummaryLayout());
			
			add(formatId(), MatchSummaryLayout.ID);
			add(formatScore(), MatchSummaryLayout.SCORE);
			add(formatPlayers(0), MatchSummaryLayout.PLAYERS_TOP);
			add(formatPlayers(1), MatchSummaryLayout.PLAYERS_BOTTOM);
			
			addMouseListener(this);
		}
		
		private JLabel formatId() {
			JLabel l=new JLabel(Integer.toString(match.getIdMatch()));
			l.setForeground(Color.GRAY);
			l.setFont(l.getFont().deriveFont(10.f));
			return l;
		}
		private String formatScoreString() {
			int[][] score=match.getScore();
			if(score==null || score.length==0) return text("placeholder.score");
			int[] sets=getSets();
			int a=sets[0];
			int b=sets[1];
			String s=a>b?"<strong>"+a+"</strong>-"+b:a+"-<strong>"+b+"</strong>";
			return "<html>"+s+"</html>";
		}
		private JLabel formatScore() {
			return new JLabel(formatScoreString());
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			dialog().setVisible(true);
		}

		@Override
		public void mousePressed(MouseEvent e) {}

		@Override
		public void mouseReleased(MouseEvent e) {}

		@Override
		public void mouseEntered(MouseEvent e) {}

		@Override
		public void mouseExited(MouseEvent e) {}
	}

	private Match match;
	
	public Match getMatch() {
		return match;
	}
	
	public MatchEntity(Match m) {
		this.match=m;
	}
	
	private static Map<Match, MatchEntity> cache;
	public static MatchEntity get(Match m) {
		if(cache==null) populateCache();
		if(!cache.containsKey(m)) cache.put(m, new MatchEntity(m));
		return cache.get(m);
	}
	private static List<MatchEntity> singlesCache; 
	public static MatchEntity getSingle(int i) {
		if(singlesCache==null) populateCache();
		if(i>=singlesCache.size()) return null;
		return singlesCache.get(i);
	}
	public static MatchEntity getSingleOrPopulate(int i) {
		if(singlesCache==null) populateCache();
		while(i>=singlesCache.size()) {
			Match m=new MatchSimple();
			Main.structs.addMatch(m);
			singlesCache.add(new MatchEntity(m));
		}
		return singlesCache.get(i);
	}
	private static List<MatchEntity> doublesCache; 
	public static MatchEntity getDouble(int i) {
		if(doublesCache==null) populateCache();
		if(i>=doublesCache.size()) return null;
		return doublesCache.get(i);
	}
	public static MatchEntity getDoubleOrPopulate(int i) {
		if(doublesCache==null) populateCache();
		while(i>=doublesCache.size()) {
			Match m=new MatchDouble();
			Main.structs.addMatch(m);
			doublesCache.add(new MatchEntity(m));
		}
		return doublesCache.get(i);
	}
	private static void populateCache() {
		List<Match> singles=FunctionalMap.toList(FunctionalMap.filter(Main.structs.getMatchs(), p -> p.b instanceof MatchSimple));
		List<Match> doubles=FunctionalMap.toList(FunctionalMap.filter(Main.structs.getMatchs(), p -> p.b instanceof MatchDouble));
		Comparator<Match> comp=Comparator.comparing(Match::getDate).thenComparing(Match::getHoraire);
		singles.sort(comp);
		doubles.sort(comp);
		singlesCache=FunctionalList.map(singles, MatchEntity::new);
		doublesCache=FunctionalList.map(doubles, MatchEntity::new);
		cache=new HashMap<>();
		singlesCache.forEach(e -> cache.put(e.match, e));
		doublesCache.forEach(e -> cache.put(e.match, e));
	}
	
	public JPanel summary() {
		return new SummaryPanel();
	}
	public JPanel details() {
		return new DetailsPanel();
	}
	public JDialog dialog() {
		return new MatchDialog(this);
	}
	
	private int[] getSets() {
		int[][] score=match.getScore();
		if(score==null || score.length==0) return new int[] {0, 0};
		int[] sets=new int[2];
		for(int [] set:score) {
			if(set[0]>set[1]) sets[0]++;
			else sets[1]++;
		}
		return sets;
	}
	private String formatPlayer(Joueur p) {
		if(p==null) return text("placeholder.player");
		return p.getNom()+" ["+p.getNationalite()+"]";
	}
	private String formatPlayersString(int idx) {
		String players;
		if(match instanceof MatchDouble) {
			DuoJoueur[] duos=((MatchDouble) match).getDuoJoueurs();
			if(duos==null || duos.length==0) {
				players=text("placeholder.player");
			} else {
				DuoJoueur duo=((MatchDouble) match).getDuoJoueurs()[idx];
				players=formatPlayer(duo.getJoueurs()[0])+" - "+formatPlayer(duo.getJoueurs()[1]);
			}
		} else {
			Joueur[] joueurs=((MatchSimple) match).getJoueurs();
			if(joueurs==null || joueurs.length==0) {
				players=text("placeholder.player");
			} else {
				players=formatPlayer(((MatchSimple) match).getJoueurs()[idx]);
			}
		}
		int[] sets=getSets();
		if(sets[idx]>sets[(idx+1)%2]) players="<strong>"+players+"</strong>";
		return "<html>"+players+"</html>";
	}
	private JLabel formatPlayers(int idx) {
		return new JLabel(formatPlayersString(idx));
	}
}
