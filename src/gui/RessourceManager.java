package gui;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

public class RessourceManager {
	public static final String locale=Locale.getDefault().getLanguage();
	private static Map<String, String> lang;
	
	private static InputStream openStream(String path) {
		InputStream stream=RessourceManager.class.getResourceAsStream(path);
		if(stream==null) throw new RuntimeException("Ressource "+path+" not found");
		return stream;
	}
	private static List<String> readLines(String path) {
		try {
			List<String> lines=new ArrayList<>();
			BufferedReader reader=new BufferedReader(new InputStreamReader(openStream(path)));
			String line;
			while((line=reader.readLine())!=null) lines.add(line);
			reader.close();
			return lines;
		} catch(IOException e) {
			throw new RuntimeException(e);
		}
	}
	private static void loadLang(String locale) {
		//XXX this is here to test locales during dev
		locale="fr";
		
		lang=new HashMap<>();
		for(String line:readLines("/lang/"+locale+".lang")) {
			String[] split=line.split("=", 2);
			if(split.length!=2) continue;
			String key=split[0];
			String value=split[1].replaceAll("\\\\n", "\n").replaceAll("\\\\t", "\t");
			lang.put(key, value);
		}
	}
	
	static {
		try {
			loadLang(locale);
		} catch(Exception e) {
			e.printStackTrace();
			try {
				loadLang("en");
			} catch(Exception ex) {
				ex.printStackTrace();
			}
		}
	}
	
	public static String text(String message) {
		return lang.getOrDefault(message, message);
	}
	public static String text(String message, Map<String, ? extends Object> replacements) {
		String text=text(message);
		for(Entry<String, ? extends Object> e:replacements.entrySet()) {
			text=text.replace("${"+e.getKey()+"}", e.getValue().toString());
		}
		return text;
	}
	
	public static ImageIcon icon(String name) {
		try {
			return new ImageIcon(ImageIO.read(openStream("/icon/"+name)));
		} catch(IOException e) {
			throw new RuntimeException(e);
		}
	}
	public static BufferedImage iconAsImage(String name) {
		try {
			return ImageIO.read(openStream("/icons/"+name));
		} catch(IOException e) {
			throw new RuntimeException(e);
		}
	}
}
