package util;

import java.util.List;
import java.util.function.Function;

public class ListUtil {
	public static <T> String concat(List<T> list, Function<? super T, String> stringifier, CharSequence sep) {
		StringBuilder sb=new StringBuilder();
		boolean first=true;
		for(T e:list) {
			if(!first) {
				sb.append(sep);
			}
			first=false;
			sb.append(stringifier.apply(e));
		}
		return sb.toString();
	}
	public static <T> String concat(List<T> list, CharSequence sep) {
		return concat(list, Object::toString, sep);
	}
}
