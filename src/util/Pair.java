package util;

public class Pair<A, B> {
	public A a;
	public B b;
	
	public Pair(A a, B b) {
		this.a=a;
		this.b=b;
	}
	
	@Override
	public boolean equals(Object other) {
		if(other==this) return true;
		if(other.getClass()!=getClass()) return false;
		return ((Pair<?, ?>) other).a==a && ((Pair<?, ?>) other).b==b;
	}
	@Override
	public int hashCode() {
		return a.hashCode()*65537+b.hashCode();
	}
	@Override
	public String toString() {
		return "("+a+","+b+")";
	}
}
