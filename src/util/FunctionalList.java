package util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class FunctionalList {
	public static <A, B> List<B> map(List<A> list, Function<? super A, ? extends B> fn) {
		List<B> dest=new ArrayList<>();
		for(A a:list) dest.add(fn.apply(a));
		return dest;
	}
	
	public static <V> Map<Integer, V> toMap(List<V> list) {
		Map<Integer, V> dest=new HashMap<>();
		int i=0;
		for(V e:list) dest.put(i++, e);
		return dest;
	}
	public static <K, V> Map<K, V> toMap(List<V> list, Function<? super V, ? extends K> keyExtractor) {
		Map<K, V> dest=new HashMap<>();
		for(V e:list) dest.put(keyExtractor.apply(e), e);
		return dest;
	}
	
	public static <A, B> B max(List<A> list, Function<? super A, ? extends B> fn, Comparator<? super B> cmp) {
		B max=fn.apply(list.get(0));
		for(A elem:list) {
			B e=fn.apply(elem);
			if(cmp.compare(max, e)<0) max=e;
		}
		return max;
	}
	public static <A, B extends Comparable<? super B>> B max(List<A> list, Function<? super A, ? extends B> fn) {
		Comparator<B> cmp=Comparator.naturalOrder();
		B max=fn.apply(list.get(0));
		for(A elem:list) {
			B e=fn.apply(elem);
			if(cmp.compare(max, e)<0) max=e;
		}
		return max;
	}
	public static <T extends Comparable<? super T>> T max(List<T> list) {
		Comparator<T> cmp=Comparator.naturalOrder();
		T max=list.get(0);
		for(T elem:list) {
			if(cmp.compare(max, elem)<0) max=elem;
		}
		return max;
	}
	public static <T> T max(List<T> list, Comparator<? super T> cmp) {
		T max=list.get(0);
		for(T elem:list) {
			if(cmp.compare(max, elem)<0) max=elem;
		}
		return max;
	}
	public static <T> void forEach(List<T> list, Consumer<? super T> fn) {
		for(T elem:list) fn.accept(elem);
	}
	public static <T> List<T> filter(List<T> list, Predicate<? super T> pred) {
		List<T> dest=new ArrayList<>();
		for(T elem:list) {
			if(pred.test(elem)) dest.add(elem);
		}
		return dest;
	}
	public static <T> List<T> flatten(List<? extends List<T>> list) {
		List<T> dest=new ArrayList<>();
		for(List<T> elem:list) dest.addAll(elem);
		return dest;
	}
}
