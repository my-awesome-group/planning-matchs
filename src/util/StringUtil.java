package util;

public class StringUtil {
	public static String rpad(Object o, int n) {
		String s=o.toString();
		int sp=n-s.length();
		StringBuilder sb=new StringBuilder();
		for(int i=0; i<sp; i++) sb.append(' ');
		sb.append(s);
		return sb.toString();
	}
	public static String zeropad(Number o, int n) {
		String s=o.toString();
		int sp=n-s.length();
		StringBuilder sb=new StringBuilder();
		for(int i=0; i<sp; i++) sb.append('0');
		sb.append(s);
		return sb.toString();
	}
}
