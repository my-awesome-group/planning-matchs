package util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.function.BiPredicate;
import java.util.function.Function;
import java.util.function.Predicate;

public class FunctionalMap {
	public static <K, V> V find(Map<K, V> map, BiPredicate<K, V> pred) {
		for(Entry<K, V> entry:map.entrySet()) {
			if(pred.test(entry.getKey(), entry.getValue())) {
				return entry.getValue();
			}
		}
		return null;
	}
	public static <Ka, Va, Kb, Vb> Map<Kb, Vb> mapKV(Map<Ka, Va> map, Function<Pair<? super Ka, ? super Va>, Pair<? extends Kb, ?extends Vb>> fn) {
		Map<Kb, Vb> dest=new HashMap<>();
		for(Entry<Ka, Va> entry:map.entrySet()) {
			Pair<Ka, Va> sP=new Pair<>(entry.getKey(), entry.getValue());
			Pair<? extends Kb, ? extends Vb> dP=fn.apply(sP);
			dest.put(dP.a, dP.b);
		}
		return dest;
	}
	public static <K, Va, Vb> Map<K, Vb> map(Map<K, Va> map, Function<? super Va, ? extends Vb> fn) {
		Map<K, Vb> dest=new HashMap<>();
		for(Entry<K, Va> entry:map.entrySet()) {
			dest.put(entry.getKey(), fn.apply(entry.getValue()));
		}
		return dest;
	}
	public static <K, V> Map<K, V> filter(Map<K, V> map, Predicate<Pair<K, V>> pred) {
		Map<K, V> dest=new HashMap<>();
		for(Entry<K, V> entry:map.entrySet()) {
			Pair<K, V> sP=new Pair<>(entry.getKey(), entry.getValue());
			if(pred.test(sP)) dest.put(entry.getKey(), entry.getValue());
		}
		return dest;
	}
	public static <V> List<V> sortToList(Map<?, V> map, Comparator<? super V> comp) {
		List<V> list=new ArrayList<>();
		list.addAll(map.values());
		list.sort(comp);
		return list;
	}
	public static <V> List<V> toList(Map<?, V> map) {
		List<V> list=new ArrayList<>();
		list.addAll(map.values());
		return list;
	}
}
