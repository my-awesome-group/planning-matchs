package util;

import java.util.HashMap;
import java.util.Map;

public class MapUtil {
	public static <K, V> Map<K, V> build(Class<K> k, Class<V> v, Object... values) {
		Map<K, V> map=new HashMap<>();
		for(int i=0; i<values.length; i+=2) {
			map.put(k.cast(values[i]), v.cast(values[i+1]));
		}
		return map;
	}
}
