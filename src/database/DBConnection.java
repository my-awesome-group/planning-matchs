package database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface DBConnection {
	Connection getConnection() throws SQLException;
	String getServerName();
	
	default PreparedStatement prepare(String sql) throws SQLException {
		return this.getConnection().prepareStatement(sql);
	}
}
