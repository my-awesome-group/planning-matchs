package database;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class MariaDBConnection implements DBConnection {
	
	private String host="localhost";
	private int port=3306;
	private String database;
	private String username;
	private String password;
	
	private Connection connection;
	
	public MariaDBConnection() {}
	public MariaDBConnection(String host, int port, String database, String username, String password) {
		this.host=host;
		this.port=port;
		this.database=database;
		this.username=username;
		this.password=password;
	}
	public MariaDBConnection(File configFile) throws FileNotFoundException, IOException {
		Properties props=new Properties();
		props.load(new FileInputStream(configFile));
		this.host=props.getProperty("host", "localhost");
		this.port=Integer.parseInt(props.getProperty("port", "3306"));
		this.database=props.getProperty("database");
		this.username=props.getProperty("username");
		this.password=props.getProperty("password");
	}
	
	public void setHost(String host) {
		if(this.connection!=null) throw new IllegalStateException("Already connected");
		this.host=host;
	}
	public void setPort(int port) {
		if(this.connection!=null) throw new IllegalStateException("Already connected");
		this.port=port;
	}
	public void setDatabase(String database) {
		if(this.connection!=null) throw new IllegalStateException("Already connected");
		this.database=database;
	}
	public void setUsername(String username) {
		if(this.connection!=null) throw new IllegalStateException("Already connected");
		this.username=username;
	}
	public void setPassword(String password) {
		if(this.connection!=null) throw new IllegalStateException("Already connected");
		this.password=password;
	}
	
	private void ensureConnection() throws SQLException {
		if(this.connection==null) {
			String connectString="jdbc:mariadb://"+this.host;
			connectString+=":"+this.port;
			connectString+="/"+this.database;
			this.connection=DriverManager.getConnection(connectString, this.username, this.password);
		}
	}
	
	@Override
	public String getServerName() {
		return "MariaDB";
	}
	
	@Override
	public Connection getConnection() throws SQLException {
		this.ensureConnection();
		return this.connection;
	}

}
