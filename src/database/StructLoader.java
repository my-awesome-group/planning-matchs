package database;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import struct.Arbitre;
import struct.ArbitreChaise;
import struct.ArbitreLigne;
import struct.Court;
import struct.DuoJoueur;
import struct.Entrainement;
import struct.EquipeRamasseurs;
import struct.Joueur;
import struct.Match;
import struct.MatchDouble;
import struct.MatchSimple;
import struct.Ramasseur;
import util.FunctionalList;
import util.FunctionalMap;

public class StructLoader {
	private PreparedStatement arbitreSelect;
	private PreparedStatement courtSelect;
	private PreparedStatement duoJoueurSelect;
	private PreparedStatement entrainementSelect;
	private PreparedStatement equipeRamasseurSelect;
	private PreparedStatement joueurSelect;
	private PreparedStatement matchSelect;
	private PreparedStatement ramasseurSelect;
	
	private PreparedStatement matchArbitreChaiseJoin;
	private PreparedStatement duoJoueurJoueurJoin;
	private PreparedStatement entrainementCourtJoin;
	private PreparedStatement entrainementJoueurJoin;
	private PreparedStatement equipeRamasseursRamasseurJoin;
	private PreparedStatement matchCourtJoin;
	private PreparedStatement matchArbitreLigneJoin;
	private PreparedStatement matchSetJoin;
	private PreparedStatement matchJoueurJoin;
	private PreparedStatement matchDuoJoueurJoin;
	private PreparedStatement matchEquipeRamasseursJoin;
	
	private Map<Integer, Arbitre> arbitres=new HashMap<>();
	private Map<Integer, Court> courts=new HashMap<>();
	private Map<Integer, Joueur> joueurs=new HashMap<>();
	private Map<Integer, DuoJoueur> duoJoueurs=new HashMap<>();
	private Map<Integer, Entrainement> entrainements=new HashMap<>();
	private Map<Integer, EquipeRamasseurs> equipesRamasseurs=new HashMap<>();
	private Map<Integer, Match> matchs=new HashMap<>();
	private Map<Integer, Ramasseur> ramasseurs=new HashMap<>();

	public StructLoader(DBConnection conn) throws SQLException {
		this.arbitreSelect=conn.prepare("SELECT * FROM arbitre");
		this.courtSelect=conn.prepare("SELECT * FROM court");
		this.duoJoueurSelect=conn.prepare("SELECT * FROM duoJoueur");
		this.entrainementSelect=conn.prepare("SELECT * FROM entrainement");
		this.equipeRamasseurSelect=conn.prepare("SELECT * FROM equipeRamasseurs");
		this.joueurSelect=conn.prepare("SELECT * FROM joueur");
		this.matchSelect=conn.prepare("SELECT * FROM matchT");
		this.ramasseurSelect=conn.prepare("SELECT * FROM ramasseur");
		
		this.matchArbitreChaiseJoin=conn.prepare("SELECT idMatch AS matchT, idArbitreChaise AS arbitre FROM matchT");
		this.duoJoueurJoueurJoin=conn.prepare("SELECT idDuoJoueur AS duo, premierJoueur AS premier, secondJoueur AS second FROM duoJoueur");
		this.entrainementCourtJoin=conn.prepare("SELECT idEntrainement AS entrainement, idCourt AS court FROM entrainement");
		this.entrainementJoueurJoin=conn.prepare("SELECT idEntrainement AS entrainement, idJoueur AS joueur FROM entrainement");
		this.equipeRamasseursRamasseurJoin=conn.prepare("SELECT idEquipeRamasseur AS equipe, idRamasseur AS ramasseur FROM ramasseur");
		this.matchCourtJoin=conn.prepare("SELECT idMatch AS matchT, idCourt AS court FROM matchT");
		this.matchArbitreLigneJoin=conn.prepare("SELECT idMatch AS matchT, idArbitre AS arbitre FROM matchArbitreLigne");
		this.matchSetJoin=conn.prepare("SELECT idMatch AS matchT, scorePremier AS premier, scoreSecond AS second FROM matchSet ORDER BY indexSet ASC");
		this.matchJoueurJoin=conn.prepare("SELECT idMatch AS matchT, premierJoueur AS premier, secondJoueur AS second FROM matchJoueur");
		this.matchDuoJoueurJoin=conn.prepare("SELECT idMatch AS matchT, premierDuo AS premier, secondDuo AS second FROM matchDuoJoueur");
		this.matchEquipeRamasseursJoin=conn.prepare("SELECT idMatch AS matchT, idEquipeRamasseur AS equipe FROM matchEquipeRamasseurs");
		
		this.loadArbitres();
		this.loadCourts();
		this.loadDuoJoueurs();
		this.loadEntrainements();
		this.loadEquipesRamasseurs();
		this.loadJoueurs();
		this.loadMatchs();
		this.loadRamasseurs();
		
		this.linkMatchArbitreChaise();
		this.linkDuoJoueurJoueur();
		this.linkEntrainementCourt();
		this.linkEntrainementJoueur();
		this.linkEquipeRamasseursRamasseur();
		this.linkMatchCourt();
		this.linkMatchArbitreLigne();
		this.linkMatchSet();
		this.linkMatchJoueur();
		this.linkMatchDuoJoueur();
		this.linkMatchEquipeRamasseurs();
	}

	private void loadArbitres() throws SQLException {
		ResultSet rst=this.arbitreSelect.executeQuery();
		while(rst.next()) {
			String categorie=rst.getString("categorie");
			Arbitre a=null;
			if(categorie.equals("ITT1")) a=new ArbitreChaise(rst);
			else if(categorie.equals("JAT2")) a=new ArbitreLigne(rst);
			else {
				System.err.printf("Arbitre %d a une catégorie %s non reconnue\n", rst.getInt("idArbitre"), categorie);
			}
			this.arbitres.put(a.getIdArbitre(), a);
		}
		rst.close();
	}

	private void loadCourts() throws SQLException {
		ResultSet rst=this.courtSelect.executeQuery();
		while(rst.next()) {
			Court c=new Court(rst);
			this.courts.put(c.getIdCourt(), c);
		}
		rst.close();
	}
	
	private void loadDuoJoueurs() throws SQLException {
		ResultSet rst=this.duoJoueurSelect.executeQuery();
		while(rst.next()) {
			DuoJoueur d=new DuoJoueur(rst);
			this.duoJoueurs.put(d.getIdDuoJoueur(), d);
		}
		rst.close();
	}
	
	private void loadEntrainements() throws SQLException {
		ResultSet rst=this.entrainementSelect.executeQuery();
		while(rst.next()) {
			Entrainement e=new Entrainement(rst);
			this.entrainements.put(e.getIdEntrainement(), e);
		}
		rst.close();
	}
	
	private void loadEquipesRamasseurs() throws SQLException {
		ResultSet rst=this.equipeRamasseurSelect.executeQuery();
		while(rst.next()) {
			EquipeRamasseurs e=new EquipeRamasseurs(rst);
			this.equipesRamasseurs.put(e.getIdEquipeRamasseur(), e);
		}
		rst.close();
	}
	
	private void loadJoueurs() throws SQLException {
		ResultSet rst=this.joueurSelect.executeQuery();
		while(rst.next()) {
			Joueur j=new Joueur(rst);
			this.joueurs.put(j.getIdJoueur(), j);
		}
		rst.close();
	}
	
	private void loadMatchs() throws SQLException {
		ResultSet rst=this.matchSelect.executeQuery();
		while(rst.next()) {
			Match m;
			if(rst.getBoolean("duo")) m=new MatchDouble(rst);
			else m=new MatchSimple(rst);
			this.matchs.put(m.getIdMatch(), m);
		}
		rst.close();
	}
	
	private void loadRamasseurs() throws SQLException {
		ResultSet rst=this.ramasseurSelect.executeQuery();
		while(rst.next()) {
			Ramasseur r=new Ramasseur(rst);
			this.ramasseurs.put(r.getIdRamasseur(), r);
		}
		rst.close();
	}
	
	private void linkMatchArbitreChaise() throws SQLException {
		ResultSet rst=this.matchArbitreChaiseJoin.executeQuery();
		while(rst.next()) {
			int match=rst.getInt("matchT");
			int arbitre=rst.getInt("arbitre");
			ArbitreChaise a=(ArbitreChaise) this.arbitres.get(arbitre);
			Match m=this.matchs.get(match);
			m.setArbitreChaise(a);
			if(m instanceof MatchSimple) a.addArbitrageSimple((MatchSimple) m);
			else a.addArbitrageDouble((MatchDouble) m);
		}
		rst.close();
	}
	
	private void linkDuoJoueurJoueur() throws SQLException {
		ResultSet rst=this.duoJoueurJoueurJoin.executeQuery();
		while(rst.next()) {
			int duo=rst.getInt("duo");
			int premier=rst.getInt("premier");
			int second=rst.getInt("second");
			DuoJoueur d=this.duoJoueurs.get(duo);
			Joueur p=this.joueurs.get(premier);
			Joueur s=this.joueurs.get(second);
			d.setJoueurs(new Joueur[] {p, s});
		}
		rst.close();
	}
	
	private void linkEntrainementCourt() throws SQLException {
		ResultSet rst=this.entrainementCourtJoin.executeQuery();
		while(rst.next()) {
			int entrainement=rst.getInt("entrainement");
			int court=rst.getInt("court");
			Entrainement e=this.entrainements.get(entrainement);
			Court c=this.courts.get(court);
			e.setCourt(c);
		}
		rst.close();
	}
	
	private void linkEntrainementJoueur() throws SQLException {
		ResultSet rst=this.entrainementJoueurJoin.executeQuery();
		while(rst.next()) {
			int entrainement=rst.getInt("entrainement");
			int joueur=rst.getInt("joueur");
			Entrainement e=this.entrainements.get(entrainement);
			Joueur j=this.joueurs.get(joueur);
			e.setJoueur(j);
		}
		rst.close();
	}
	
	private void linkEquipeRamasseursRamasseur() throws SQLException {
		ResultSet rst=this.equipeRamasseursRamasseurJoin.executeQuery();
		while(rst.next()) {
			int equipe=rst.getInt("equipe");
			int ramasseur=rst.getInt("ramasseur");
			EquipeRamasseurs e=this.equipesRamasseurs.get(equipe);
			Ramasseur r=this.ramasseurs.get(ramasseur);
			e.addRamasseur(r);
			r.setEquipe(e);
		}
		rst.close();
	}
	
	private void linkMatchCourt() throws SQLException {
		ResultSet rst=this.matchCourtJoin.executeQuery();
		while(rst.next()) {
			int match=rst.getInt("matchT");
			int court=rst.getInt("court");
			Match m=this.matchs.get(match);
			Court c=this.courts.get(court);
			m.setCourt(c);
		}
		rst.close();
	}
	
	private void linkMatchArbitreLigne() throws SQLException {
		ResultSet rst=this.matchArbitreLigneJoin.executeQuery();
		while(rst.next()) {
			int match=rst.getInt("matchT");
			int arbitre=rst.getInt("arbitre");
			Match m=this.matchs.get(match);
			ArbitreLigne a=(ArbitreLigne) this.arbitres.get(arbitre);
			m.addArbitreLigne(a);
		}
		rst.close();
	}
	
	private void linkMatchSet() throws SQLException {
		ResultSet rst=this.matchSetJoin.executeQuery();
		while(rst.next()) {
			int match=rst.getInt("matchT");
			int premier=rst.getInt("premier");
			int second=rst.getInt("second");
			Match m=this.matchs.get(match);
			m.addScore(new int[] {premier, second});
		}
		rst.close();
	}
	
	private void linkMatchJoueur() throws SQLException {
		ResultSet rst=this.matchJoueurJoin.executeQuery();
		while(rst.next()) {
			int match=rst.getInt("matchT");
			int premier=rst.getInt("premier");
			int second=rst.getInt("second");
			MatchSimple m=(MatchSimple) this.matchs.get(match);
			Joueur p=this.joueurs.get(premier);
			Joueur s=this.joueurs.get(second);
			m.setJoueurs(new Joueur[] {p, s});
		}
		rst.close();
	}
	
	private void linkMatchDuoJoueur() throws SQLException {
		ResultSet rst=this.matchDuoJoueurJoin.executeQuery();
		while(rst.next()) {
			int match=rst.getInt("matchT");
			int premier=rst.getInt("premier");
			int second=rst.getInt("second");
			MatchDouble m=(MatchDouble) this.matchs.get(match);
			DuoJoueur p=this.duoJoueurs.get(premier);
			DuoJoueur s=this.duoJoueurs.get(second);
			m.setDuoJoueurs(new DuoJoueur[] {p, s});
		}
		rst.close();
	}
	
	private void linkMatchEquipeRamasseurs() throws SQLException {
		ResultSet rst=this.matchEquipeRamasseursJoin.executeQuery();
		while(rst.next()) {
			int match=rst.getInt("matchT");
			int equipe=rst.getInt("equipe");
			Match m=this.matchs.get(match);
			EquipeRamasseurs e=this.equipesRamasseurs.get(equipe);
			m.setEquipeRamasseurs(e);
		}
		rst.close();
	}

	/**
	 * @return the arbitres
	 */
	public Map<Integer, Arbitre> getArbitres() {
		return arbitres;
	}

	/**
	 * @param arbitres the arbitres to set
	 */
	public void setArbitres(Map<Integer, Arbitre> arbitres) {
		this.arbitres = arbitres;
	}

	/**
	 * @return the courts
	 */
	public Map<Integer, Court> getCourts() {
		return courts;
	}

	/**
	 * @param courts the courts to set
	 */
	public void setCourts(Map<Integer, Court> courts) {
		this.courts = courts;
	}

	/**
	 * @return the joueurs
	 */
	public Map<Integer, Joueur> getJoueurs() {
		return joueurs;
	}

	/**
	 * @param joueurs the joueurs to set
	 */
	public void setJoueurs(Map<Integer, Joueur> joueurs) {
		this.joueurs = joueurs;
	}

	/**
	 * @return the duoJoueurs
	 */
	public Map<Integer, DuoJoueur> getDuoJoueurs() {
		return duoJoueurs;
	}

	/**
	 * @param duoJoueurs the duoJoueurs to set
	 */
	public void setDuoJoueurs(Map<Integer, DuoJoueur> duoJoueurs) {
		this.duoJoueurs = duoJoueurs;
	}

	/**
	 * @return the entrainements
	 */
	public Map<Integer, Entrainement> getEntrainements() {
		return entrainements;
	}

	/**
	 * @param entrainements the entrainements to set
	 */
	public void setEntrainements(Map<Integer, Entrainement> entrainements) {
		this.entrainements = entrainements;
	}

	/**
	 * @return the equipesRamasseurs
	 */
	public Map<Integer, EquipeRamasseurs> getEquipesRamasseurs() {
		return equipesRamasseurs;
	}

	/**
	 * @param equipesRamasseurs the equipesRamasseurs to set
	 */
	public void setEquipesRamasseurs(Map<Integer, EquipeRamasseurs> equipesRamasseurs) {
		this.equipesRamasseurs = equipesRamasseurs;
	}

	/**
	 * @return the matchs
	 */
	public Map<Integer, Match> getMatchs() {
		return matchs;
	}

	/**
	 * @param matchs the matchs to set
	 */
	public void setMatchs(Map<Integer, Match> matchs) {
		this.matchs = matchs;
	}
	
	public void addMatch(Match m) {
		if(m.getIdMatch()==0) {
			if(matchs.isEmpty()) m.setIdMatch(1);
			else m.setIdMatch(FunctionalList.max(FunctionalMap.toList(matchs), Match::getIdMatch)+1);
		}
		this.matchs.put(m.getIdMatch(), m);
	}

	/**
	 * @return the ramasseurs
	 */
	public Map<Integer, Ramasseur> getRamasseurs() {
		return ramasseurs;
	}

	/**
	 * @param ramasseurs the ramasseurs to set
	 */
	public void setRamasseurs(Map<Integer, Ramasseur> ramasseurs) {
		this.ramasseurs = ramasseurs;
	}

}
