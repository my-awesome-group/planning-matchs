package struct;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.Date;

public class MatchSimple extends Match {
	protected Joueur[] joueurs;
	
	public MatchSimple() {
		super();
	}
	public MatchSimple(int idMatch, Date date, int horaire, int[][] score, int indexGagnant, Court court, ArbitreChaise arbitreChaise, ArbitreLigne[] arbitresLigne, Joueur[] joueurs) {
		super(idMatch, date, horaire, score, indexGagnant, court, arbitreChaise, arbitresLigne);
		this.joueurs=Arrays.copyOf(joueurs, joueurs.length);
	}
	public MatchSimple(MatchSimple m) {
		this(m.idMatch, m.date, m.horaire, m.score, m.indexGagnant, m.court, m.arbitreChaise, m.arbitresLigne, m.joueurs);
	}
	public MatchSimple(ResultSet r) throws SQLException {
		super(r);
	}
	
	public Joueur[] getJoueurs() {
		return joueurs==null?null:Arrays.copyOf(joueurs, joueurs.length);
	}
	public void setJoueurs(Joueur[] joueurs) {
		this.joueurs=Arrays.copyOf(joueurs, joueurs.length);
	}
}
