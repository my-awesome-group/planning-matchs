package struct;

import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class Arbitre {
	protected int idArbitre;
	protected String nom;
	protected String prenom;
	protected String nationalite;
	protected String categorie;
	
	public Arbitre() {
	}
	public Arbitre(int idArbitre, String nom, String prenom, String nationalite, String categorie) {
		this.idArbitre=idArbitre;
		this.nom=nom;
		this.prenom=prenom;
		this.nationalite=nationalite;
		this.categorie=categorie;
	}
	public Arbitre(Arbitre a) {
		this(a.idArbitre, a.nom, a.prenom, a.nationalite, a.categorie);
	}
	public Arbitre(ResultSet r) throws SQLException {
		this.idArbitre=r.getInt("idArbitre");
		this.nom=r.getString("nom");
		this.prenom=r.getString("prenom");
		this.nationalite=r.getString("nationalite");
		this.categorie=r.getString("categorie");
	}
	
	public int getIdArbitre() {
		return idArbitre;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getNationalite() {
		return nationalite;
	}
	public void setNationalite(String nationalite) {
		this.nationalite = nationalite;
	}
	public String getCategorie() {
		return categorie;
	}
	public void setCategorie(String categorie) {
		this.categorie = categorie;
	}
}
