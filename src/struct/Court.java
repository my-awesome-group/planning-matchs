package struct;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Court {
	protected int idCourt;
	protected boolean principal;
	
	public Court() {
	}
	public Court(int idCourt, boolean principal) {
		this.idCourt=idCourt;
		this.principal=principal;
	}
	public Court(Court c) {
		this(c.idCourt, c.principal);
	}
        
        public Court(ResultSet rst) throws SQLException {
            this.idCourt=rst.getInt("idCourt");
            this.principal=rst.getBoolean("principal");
        }
	
	public int getIdCourt() {
		return idCourt;
	}
	public boolean isPrincipal() {
		return principal;
	}
	public void setPrincipal(boolean principal) {
		this.principal = principal;
	}
}
