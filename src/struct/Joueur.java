package struct;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Joueur {
	protected int idJoueur;
	protected String nom;
	protected String prenom;
	protected int atp;
	protected String nationalite;
	
	public Joueur() {
	}
	public Joueur(int idJoueur, String nom, String prenom, int atp, String nationalite) {
		this.idJoueur=idJoueur;
		this.nom=nom;
		this.prenom=prenom;
		this.atp=atp;
		this.nationalite=nationalite;
	}
	public Joueur(Joueur j) {
		this(j.idJoueur, j.nom, j.prenom, j.atp, j.nationalite);
	}
        
        public Joueur(ResultSet rst) throws SQLException{
            this.idJoueur=rst.getInt("idJoueur");
            this.nom=rst.getString("nom");
            this.prenom=rst.getString("prenom");
            this.atp=rst.getInt("atp");
            this.nationalite=rst.getString("nationalite");
        }
	
	public int getIdJoueur() {
		return idJoueur;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public int getAtp() {
		return atp;
	}
	public void setAtp(int atp) {
		this.atp = atp;
	}
	public String getNationalite() {
		return nationalite;
	}
	public void setNationalite(String nationalite) {
		this.nationalite = nationalite;
	}
}
