package struct;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.Arrays;

public abstract class Match {
	protected int idMatch;
	protected Date date;
	protected int horaire;
	protected int[][] score=new int[][] {};
	protected int indexGagnant;
	protected Court court;
	protected ArbitreChaise arbitreChaise;
	protected ArbitreLigne[] arbitresLigne=new ArbitreLigne[] {};
	protected EquipeRamasseurs equipeRamasseurs;
	
	public EquipeRamasseurs getEquipeRamasseurs() {
		return equipeRamasseurs;
	}
	public void setEquipeRamasseurs(EquipeRamasseurs equipeRamasseurs) {
		this.equipeRamasseurs = equipeRamasseurs;
	}
	public Match() {
	}
	public Match(int idMatch) {
		this.idMatch=idMatch;
	}
	public Match(int idMatch, Date date, int horaire, int[][] score, int indexGagnant, Court court, ArbitreChaise arbitreChaise, ArbitreLigne[] arbitresLigne) {
		this.idMatch=idMatch;
		this.date=(Date) date.clone();
		this.horaire=horaire;
		this.score=Arrays.copyOf(score, score.length);
		this.indexGagnant=indexGagnant;
		this.court=court;
		this.arbitreChaise=arbitreChaise;
		this.arbitresLigne=Arrays.copyOf(arbitresLigne, arbitresLigne.length);
	}
	public Match(Match m) {
		this(m.idMatch, m.date, m.horaire, m.score, m.indexGagnant, m.court, m.arbitreChaise, m.arbitresLigne);
	}
	
    public Match(ResultSet rst) throws SQLException{
        this.idMatch=rst.getInt("idMatch");
        this.date=rst.getDate("dateMatch");
        this.horaire=rst.getInt("horaire");
        this.indexGagnant=rst.getInt("indexGagnant");
    }
        
	public int getIdMatch() {
		return this.idMatch;
	}
	public void setIdMatch(int id) {
		this.idMatch=id;
	}
	public Date getDate() {
		return this.date;
	}
	public void setDate(Date date) {
		this.date=date;
	}
	public int getHoraire() {
		return this.horaire;
	}
	public void setHoraire(int horaire) {
		this.horaire=horaire;
	}
	public int[][] getScore() {
		return Arrays.copyOf(this.score, this.score.length);
	}
	public void setScore(int[][] score) {
		this.score=Arrays.copyOf(score, score.length);
	}
	public void addScore(int[] score) {
		this.score=Arrays.copyOf(this.score, this.score.length+1);
		this.score[this.score.length-1]=score;
	}
	public int getIndexGagnant() {
		return this.indexGagnant;
	}
	public void setIndexGagnant(int indexGagnant) {
		this.indexGagnant=indexGagnant;
	}
	public Court getCourt() {
		return court;
	}
	public void setCourt(Court court) {
		this.court = court;
	}
	public ArbitreChaise getArbitreChaise() {
		return arbitreChaise;
	}
	public void setArbitreChaise(ArbitreChaise arbitreChaise) {
		this.arbitreChaise = arbitreChaise;
	}
	public ArbitreLigne[] getArbitresLigne() {
		return Arrays.copyOf(arbitresLigne, arbitresLigne.length);
	}
	public void setArbitresLigne(ArbitreLigne[] arbitresLigne) {
		this.arbitresLigne=Arrays.copyOf(arbitresLigne, arbitresLigne.length);
	}
	public void addArbitreLigne(ArbitreLigne arbitreLigne) {
		this.arbitresLigne=Arrays.copyOf(this.arbitresLigne, this.arbitresLigne.length+1);
		this.arbitresLigne[this.arbitresLigne.length-1]=arbitreLigne;
	}
	public abstract Joueur[] getJoueurs();
}

