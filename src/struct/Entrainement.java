package struct;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class Entrainement {
	protected int idEntrainement;
	protected Date date;
	protected double heureDebut;
	protected double heureFin;
	protected Joueur joueur;
	protected Court court;
	
	public Entrainement() {
	}
	public Entrainement(int idEntrainement, Date date, double heureDebut, double heureFin, Joueur joueur, Court court) {
		this.idEntrainement=idEntrainement;
		this.date=(Date) date.clone();
		this.heureDebut=heureDebut;
		this.heureFin=heureFin;
		this.joueur=joueur;
		this.court=court;
	}
	public Entrainement(Entrainement e) {
		this(e.idEntrainement, e.date, e.heureDebut, e.heureFin, e.joueur, e.court);
	}
	public Entrainement(ResultSet r) throws SQLException {
		this.idEntrainement=r.getInt("idEntrainement");
		this.date=r.getDate("dateEntrainement");
		this.heureDebut=r.getDouble("heureDebut");
		this.heureFin=r.getDouble("heureFin");
	}
	
	public int getIdEntrainement() {
		return idEntrainement;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public double getHeureDebut() {
		return heureDebut;
	}
	public void setHeureDebut(double heureDebut) {
		this.heureDebut = heureDebut;
	}
	public double getHeureFin() {
		return heureFin;
	}
	public void setHeureFin(double heureFin) {
		this.heureFin = heureFin;
	}
	public Joueur getJoueur() {
		return joueur;
	}
	public void setJoueur(Joueur joueur) {
		this.joueur = joueur;
	}
	public Court getCourt() {
		return court;
	}
	public void setCourt(Court court) {
		this.court = court;
	}
}
