package struct;

import java.util.Collections;
import java.util.Date;
import java.util.Set;
import java.util.HashSet;

public class Planning {
	protected int idPlanning;
	protected Date dateDebut;
	protected Date dateFin;
	protected String libelle;
	protected Set<Match> matches=new HashSet<>();
	
	public Planning() {
	}
	public Planning(int idPlanning, Date dateDebut, Date dateFin, String libelle, Set<Match> matches) {
		this.idPlanning=idPlanning;
		this.dateDebut=dateDebut;
		this.dateFin=dateFin;
		this.libelle=libelle;
		this.matches.addAll(matches);
	}
	public Planning(Planning p) {
		this(p.idPlanning, p.dateDebut, p.dateFin, p.libelle, p.matches);
	}
	public int getIdPlanning() {
		return idPlanning;
	}
	public Date getDateDebut() {
		return (Date) dateDebut.clone();
	}
	public void setDateDebut(Date dateDebut) {
		this.dateDebut = (Date) dateDebut.clone();
	}
	public Date getDateFin() {
		return (Date) dateFin.clone();
	}
	public void setDateFin(Date dateFin) {
		this.dateFin = (Date) dateFin.clone();
	}
	public String getLibelle() {
		return libelle;
	}
	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}
	public Set<Match> getMatches() {
		return Collections.unmodifiableSet(matches);
	}
	public void setMatches(Set<Match> matches) {
		this.matches = new HashSet<>(matches);
	}
}
