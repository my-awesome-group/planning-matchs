package struct;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

public class DuoJoueur {
	protected int idDuoJoueur;
	protected Joueur[] joueurs;
	
	public DuoJoueur() {
	}
	public DuoJoueur(int idDuoJoueur, Joueur[] joueurs) {
		this.idDuoJoueur=idDuoJoueur;
		this.joueurs=Arrays.copyOf(joueurs, joueurs.length);
	}
	public DuoJoueur(DuoJoueur d) {
		this(d.idDuoJoueur, d.joueurs);
	}
	public DuoJoueur(ResultSet rst) throws SQLException {
		this.idDuoJoueur=rst.getInt("idDuoJoueur");
	}
	
	public int getIdDuoJoueur() {
		return idDuoJoueur;
	}
	public Joueur[] getJoueurs() {
		return joueurs==null?null:Arrays.copyOf(joueurs, joueurs.length);
	}
	public void setJoueurs(Joueur[] joueurs) {
		this.joueurs=Arrays.copyOf(joueurs, joueurs.length);
	}
}
