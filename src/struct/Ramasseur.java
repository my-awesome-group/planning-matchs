package struct;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Ramasseur {
	protected int idRamasseur;
	protected String nom;
	protected String prenom;
	protected EquipeRamasseurs equipe;
	
	public Ramasseur() {
	}
	public Ramasseur(int idRamasseur, String nom, String prenom, EquipeRamasseurs equipe) {
		this.idRamasseur=idRamasseur;
		this.nom=nom;
		this.prenom=prenom;
		this.equipe=equipe;
	}
	public Ramasseur(Ramasseur r) {
		this(r.idRamasseur, r.nom, r.prenom, r.equipe);
	}
        
        public Ramasseur(ResultSet rst) throws SQLException{
            this.idRamasseur=rst.getInt("idRamasseur");
            this.nom=rst.getString("nom");
            this.prenom=rst.getString("prenom");
        }
	
	public int getIdRamasseur() {
		return idRamasseur;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public EquipeRamasseurs getEquipe() {
		return equipe;
	}
	public void setEquipe(EquipeRamasseurs equipe) {
		this.equipe = equipe;
	}
}
