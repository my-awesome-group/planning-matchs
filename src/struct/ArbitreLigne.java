package struct;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ArbitreLigne extends Arbitre {
	public ArbitreLigne() {
		super();
	}
	public ArbitreLigne(int idArbitre, String nom, String prenom, String nationalite, String categorie) {
		super(idArbitre, nom, prenom, nationalite, categorie);
	}
	public ArbitreLigne(Arbitre a) {
		super(a);
	}
	public ArbitreLigne(ResultSet r) throws SQLException {
		super(r);
	}
}
