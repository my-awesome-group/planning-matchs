package struct;

import java.util.Date;

import util.FunctionalList;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

public class MatchDouble extends Match {
	protected DuoJoueur[] joueurs;
	
	public MatchDouble() {
	}
	public MatchDouble(int idMatch, Date date, int horaire, int[][] score, int indexGagnant, Court court, ArbitreChaise arbitreChaise, ArbitreLigne[] arbitresLigne, DuoJoueur[] joueurs) {
		super(idMatch, date, horaire, score, indexGagnant, court, arbitreChaise, arbitresLigne);
		this.joueurs=Arrays.copyOf(joueurs, joueurs.length);
	}
	public MatchDouble(MatchDouble m) {
		this(m.idMatch, m.date, m.horaire, m.score, m.indexGagnant, m.court, m.arbitreChaise, m.arbitresLigne, m.joueurs);
	}
	public MatchDouble(ResultSet r) throws SQLException {
		super(r);
	}
	
	public DuoJoueur[] getDuoJoueurs() {
		return joueurs==null?null:Arrays.copyOf(joueurs, joueurs.length);
	}
	public void setDuoJoueurs(DuoJoueur[] joueurs) {
		this.joueurs=Arrays.copyOf(joueurs, joueurs.length);
	}
	
	public Joueur[] getJoueurs() {
		return FunctionalList.flatten(FunctionalList.map(Arrays.asList(getDuoJoueurs()), d -> Arrays.asList(d.getJoueurs()))).toArray(new Joueur[0]);
	}
}
