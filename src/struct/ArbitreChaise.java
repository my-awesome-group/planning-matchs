package struct;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

public class ArbitreChaise extends Arbitre {
	protected MatchSimple[] arbitragesSimples=new MatchSimple[] {};
	protected MatchDouble[] arbitragesDoubles=new MatchDouble[] {};
	
	public ArbitreChaise() {
		super();
	}
	public ArbitreChaise(int idArbitre, String nom, String prenom, String nationalite, String categorie, MatchSimple[] arbitragesSimples, MatchDouble[] arbitragesDoubles) {
		super(idArbitre, nom, prenom, nationalite, categorie);
		this.arbitragesSimples=Arrays.copyOf(arbitragesSimples, arbitragesSimples.length);
		this.arbitragesDoubles=Arrays.copyOf(arbitragesDoubles, arbitragesDoubles.length);
	}
	public ArbitreChaise(ArbitreChaise a) {
		this(a.idArbitre, a.nom, a.prenom, a.nationalite, a.categorie, a.arbitragesSimples, a.arbitragesDoubles);
	}
	public ArbitreChaise(ResultSet r) throws SQLException {
		super(r);
	}
	
	public MatchSimple[] getArbitragesSimples() {
		return Arrays.copyOf(arbitragesSimples, arbitragesSimples.length);
	}
	public void setArbitragesSimples(MatchSimple[] arbitragesSimples) {
		this.arbitragesSimples = Arrays.copyOf(arbitragesSimples, arbitragesSimples.length);
	}
	public void addArbitrageSimple(MatchSimple arbitrageSimple) {
		this.arbitragesSimples=Arrays.copyOf(this.arbitragesSimples, this.arbitragesSimples.length+1);
		this.arbitragesSimples[this.arbitragesSimples.length-1]=arbitrageSimple;
	}
	public MatchDouble[] getArbitragesDoubles() {
		return Arrays.copyOf(arbitragesDoubles, arbitragesDoubles.length);
	}
	public void setArbitragesDoubles(MatchDouble[] arbitragesDoubles) {
		this.arbitragesDoubles = Arrays.copyOf(arbitragesDoubles, arbitragesDoubles.length);
	}
	public void addArbitrageDouble(MatchDouble arbitrageDouble) {
		this.arbitragesDoubles=Arrays.copyOf(this.arbitragesDoubles, this.arbitragesDoubles.length+1);
		this.arbitragesDoubles[this.arbitragesDoubles.length-1]=arbitrageDouble;
	}
}
