package struct;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;

public class EquipeRamasseurs {
	protected int idEquipeRamasseur;
	protected Ramasseur[] ramasseurs=new Ramasseur[] {};
	
	public EquipeRamasseurs() {
	}
	public EquipeRamasseurs(int idEquipeRamasseur, Ramasseur[] ramasseurs) {
		this.idEquipeRamasseur=idEquipeRamasseur;
		this.ramasseurs=Arrays.copyOf(ramasseurs, ramasseurs.length);
	}
	public EquipeRamasseurs(EquipeRamasseurs e) {
		this(e.idEquipeRamasseur, e.ramasseurs);
	}
	public EquipeRamasseurs(ResultSet rst) throws SQLException {
		this.idEquipeRamasseur=rst.getInt("idEquipeRamasseur");
	}
	
	public int getIdEquipeRamasseur() {
		return idEquipeRamasseur;
	}
	public Ramasseur[] getRamasseurs() {
		return Arrays.copyOf(ramasseurs, ramasseurs.length);
	}
	public void setRamasseurs(Ramasseur[] ramasseurs) {
		this.ramasseurs = Arrays.copyOf(ramasseurs, ramasseurs.length);
	}
	public void addRamasseur(Ramasseur ramasseur) {
		this.ramasseurs=Arrays.copyOf(this.ramasseurs, this.ramasseurs.length+1);
		this.ramasseurs[this.ramasseurs.length-1]=ramasseur;
	}
}
